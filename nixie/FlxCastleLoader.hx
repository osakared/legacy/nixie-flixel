/* 
 * Copyright 2017 Thomas J. Webb
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *         following disclaimer.
 *      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *         following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package nixie;

import cdb.Data;
import cdb.Types;
import flash.display.BitmapData;
import flixel.FlxObject;
import flixel.FlxSprite;
// import flixel.math.FlxPoint;
import flixel.tile.FlxTilemap;
import haxe.io.Bytes;
import openfl.Assets;
import openfl.geom.Point;
import openfl.geom.Rectangle;
using StringTools;

typedef TileMap = {
    var map:Array<Array<Int>>;
    var tileSize:Int;
    var fileName:String;
}

typedef MapObject = {
    var x:Int;
    var y:Int;
    var tileIndex:Int;
}

typedef ObjectMap = {
    var tileSize:Int;
    var fileName:String;
    var objects:Array<MapObject>;
}

typedef TileProps = {
    var collide:String;
}

typedef TileSet = {
    var x:Int;
    var y:Int;
    var w:Int;
    var h:Int;
}

class FlxCastleLoader
{
    private var data:Dynamic;
    private var maps:Map<String, TileMap>; // tile layers
    private var objectLayers:Map<String, ObjectMap>;
    private var hitMap:TileMap; // 0 - no hit, 1 - hit
    private var tilesPropsPerFile:Map<String, Array<TileProps>>;
    private var tilesSetsPerFile:Map<String, Array<TileSet>>;
    private var mapWidth:Int;
    private var mapHeight:Int;

    private static inline var HIT_DIVISIONS:Int = 4;

    public function new(_data:Dynamic)
    {
        data = _data;
        maps = new Map<String, TileMap>();
        objectLayers = new Map<String, ObjectMap>();
        hitMap = {map: new Array<Array<Int>>(), tileSize: 0, fileName: ""};
        tilesPropsPerFile = new Map<String, Array<TileProps>>();
        tilesSetsPerFile = new Map<String, Array<TileSet>>();

        var levelData = data.levelData;

        // Just grab the first one for now
		var level = levelData.all[0];
        mapWidth = level.width;
        mapHeight = level.height;

        var layers:Array<TileLayer> = level.layers;
        for (i in 0...layers.length) {
            addTileMap(layers[i]);
        }

        initTilePropsAndSets(levelData);
        createHitTileMap();
    }

    private function initTilePropsAndSets(levelData:Dynamic):Void
    {
        var props:Dynamic<TilesetProps> = levelData.sheet.props.level.tileSets;

        for (field in Reflect.fields(props)) {
            var tilesetProps:TilesetProps = Reflect.field(props, field);

            var tilesProps:Array<Dynamic> = tilesetProps.props;
            var newTilesProps = new Array<TileProps>();
            for (i in 0...tilesProps.length) {
                var tileProps:TileProps = tilesProps[i];
                if (tileProps == null) {
                    tileProps = {collide: "None"};
                }
                newTilesProps.push(tileProps);
            }
            tilesPropsPerFile.set(field, newTilesProps);
            tilesSetsPerFile.set(field, tilesetProps.sets);            
        }
    }

    private function addTileMap(layer:Dynamic):Void
    {
        var mapData = new Array<Array<Int>>();

        var mapRawData = cdb.Lz4Reader.decodeString(layer.data.data);
        if (mapRawData.getUInt16(0) == 0xffff) {
            addObjectLayer(layer, mapRawData);
            return;
        }
        if (mapRawData.length>>1 < mapWidth * mapHeight) {
            trace("Bad dimensions on map: " + (mapRawData.length>>1) + " != " + (mapWidth * mapHeight));
            return;
        }
        var i = 0;
        for (y in 0...mapHeight) {
            mapData[y] = new Array<Int>();
            for (x in 0...mapWidth) {
                i = y * mapWidth + x;
                mapData[y][x] = mapRawData.get(i<<1) | (mapRawData.get((i<<1)+1) << 8);
            }
        }

        maps.set(layer.name, {tileSize: layer.data.size, fileName: layer.data.file, map: mapData});
    }

    private function addObjectLayer(layer:Dynamic, rawData:Bytes)
    {
        var numObjects = rawData.length>>1;
        numObjects = Std.int((numObjects - 1) / 3);
        var mapObjects = new Array<MapObject>();
        for (i in 0...numObjects) {
            var x = rawData.getUInt16(i*6+2);
            var y = rawData.getUInt16(i*6+4);
            var tileIndex = rawData.getUInt16(i*6+6);
            mapObjects.push({x: x, y: y, tileIndex: tileIndex});
        }
        objectLayers.set(layer.name, {tileSize: layer.data.size, fileName: layer.data.file, objects: mapObjects});
    }

    // Create an invisible layer used just for collision with smaller tiles
    private function createHitTileMap():Void
    {
        // Initialize the Arrays... wish I could pass a vector into loadMap...
        for (x in 0...mapWidth * HIT_DIVISIONS) {
            hitMap.map[x] = new Array<Int>();
        }

        var tileSize = 0;
        for (map in maps) {
            if (tileSize == 0) tileSize = map.tileSize;
            // else assert(tileSize == map.tileSize);
        }
        // assert(tileSize > 0);
        // assert(tileSize % HIT_DIVISIONS == 0);
        hitMap.tileSize = Std.int(tileSize / HIT_DIVISIONS);

        for (x in 0...mapWidth) {
            for (y in 0...mapHeight) {
                // Determine hit area type. Full overrides except No overrides everything
                // Bit fields for which sub-tiles are a hit
                var hitTiles:Int = 0;
                for (map in maps) {
                    var tileIndex = map.map[y][x];
                    if (tileIndex > 0) tileIndex -= 1;
                    var fileProps = tilesPropsPerFile[map.fileName];
                    if (fileProps == null) {
                        continue;
                    }
                    var tileProps = fileProps[tileIndex];
                    if (tileProps == null) {
                        continue;
                    }
                    // "No" trumps all.. think a bridge over water
                    if (tileProps.collide == "No") {
                        hitTiles = 0;
                        break;
                    }
                    // Trumps all but "No"
                    else if (tileProps.collide == "Full") {
                        hitTiles = 0xffff;
                        break;
                    }
                    else if (tileProps.collide == "None" || tileProps.collide == null) {
                        continue;
                    }
                    else if (tileProps.collide == "Left") {
                        hitTiles |= 0x8888;
                    }
                    else if (tileProps.collide == "Right") {
                        hitTiles |= 0x1111;
                    }
                    else if (tileProps.collide == "Top") {
                        hitTiles |= 0xf000;
                    }
                    else if (tileProps.collide == "Bottom") {
                        hitTiles |= 0xf;
                    }
                    else if (tileProps.collide == "CornerTL") {
                        hitTiles |= 0xf888;
                    }
                    else if (tileProps.collide == "CornerTR") {
                        hitTiles |= 0xf111;
                    }
                    else if (tileProps.collide == "CornerBR") {
                        hitTiles |= 0x111f;
                    }
                    else if (tileProps.collide == "CornerBL") {
                        hitTiles |= 0x888f;
                    }
                    else if (tileProps.collide == "Small") {
                        hitTiles |= 0x660;
                    }
                    else if (tileProps.collide == "HalfTop") {
                        hitTiles |= 0xff00;
                    }
                    else if (tileProps.collide == "HalfRight") {
                        hitTiles |= 0x3333;
                    }
                    else if (tileProps.collide == "HalfBottom") {
                        hitTiles |= 0xff;
                    }
                    else if (tileProps.collide == "HalfLeft") {
                        hitTiles |= 0xcccc;
                    }
                    else if (tileProps.collide == "SmallTR") {
                        hitTiles |= 0x3300;
                    }
                    else if (tileProps.collide == "SmallTL") {
                        hitTiles |= 0xcc00;
                    }
                    else if (tileProps.collide == "SmallBL") {
                        hitTiles |= 0xcc;
                    }
                    else if (tileProps.collide == "SmallBR") {
                        hitTiles |= 0x33;
                    }
                    else trace(tileProps.collide);
                }

                for (subX in 0...HIT_DIVISIONS) {
                    for (subY in 0...HIT_DIVISIONS) {
                        var tileValue = (hitTiles >> (0xf - (subX * HIT_DIVISIONS + subY))) & 1;
                        hitMap.map[y*HIT_DIVISIONS+subY][x*HIT_DIVISIONS+subX] = tileValue;
                    }
                }
            }
        }
    }

    public function loadHitMap():FlxTilemap
    {
        var tileMap = new FlxTilemap();
        var emptyTiles = new BitmapData(hitMap.tileSize * 2, hitMap.tileSize, true, 0);
        // emptyTiles.fillRect(new Rectangle(hitMap.tileSize, 0, hitMap.tileSize, hitMap.tileSize), 0x77ff0000); // Uncomment to debug
        tileMap.loadMapFrom2DArray(hitMap.map, emptyTiles, hitMap.tileSize, hitMap.tileSize);
        return tileMap;
    }

    public function loadTilemap(tileLayer:String):FlxTilemap
    {
        if (!maps.exists(tileLayer)) {
            trace('layer not found: ' + tileLayer);
            return null;
        }
        var mapInfo = maps.get(tileLayer);
        var tileMap = new FlxTilemap();
        tileMap.loadMapFrom2DArray(mapInfo.map, "assets/images/" + mapInfo.fileName, mapInfo.tileSize, mapInfo.tileSize, OFF, 1);
        return tileMap;
    }

    public function loadObjects(objectLayerName:String, placeObjectFn:FlxSprite->Void, placeHitObjectFn:FlxObject->Void):Void
    {
        if (!objectLayers.exists(objectLayerName)) {
            trace('layer not found: ' + objectLayerName);
            return;
        }

        var objectLayer = objectLayers.get(objectLayerName);
        var rawData:BitmapData = Assets.getBitmapData("assets/images/" + objectLayer.fileName);

        var tileSize = objectLayer.tileSize;
        var widthInTiles:Int = Math.floor(rawData.width / tileSize);
        var heightInTiles:Int = Math.floor(rawData.height / tileSize);
        
        var tileSets:Array<TileSet> = tilesSetsPerFile[objectLayer.fileName];
        var mapObjects:Array<MapObject> = objectLayer.objects;
        
        for (i in 0...mapObjects.length) {
            var mapObject:MapObject = mapObjects[i];
            var object = new FlxSprite(mapObject.x, mapObject.y);
            var tileSet:TileSet = null;
            for (j in 0...tileSets.length) {
                // converting coordinates into index
                if (tileSets[j].x + tileSets[j].y * widthInTiles == mapObject.tileIndex) {
                    tileSet = tileSets[j];
                    break;
                }
            }
            if (tileSet == null) {
                trace("Couldn't find tileSet for layer");
                break;
            }

            object.pixels = new BitmapData(tileSet.w * tileSize, tileSet.h * tileSize);
            object.pixels.copyPixels(rawData, new Rectangle(tileSet.x * tileSize, tileSet.y * tileSize, tileSet.w * tileSize, tileSet.h * tileSize), new Point(0, 0));
            placeObjectFn(object);

            var fileProps = tilesPropsPerFile[objectLayer.fileName];
            if (fileProps == null) {
                continue;
            }

            for (x in 0...tileSet.w) {
                for (y in 0...tileSet.h) {
                    var tileIndex = x + y * widthInTiles + mapObject.tileIndex;
                    var tileProps = fileProps[tileIndex];
                    if (tileProps == null) {
                        continue;
                    }
                    if (tileProps.collide == "Null" || tileProps.collide == "No" || tileProps.collide == null) {
                        continue;
                    }

                    if (tileProps.collide.startsWith("Corner")) {
                        var hitX1 = mapObject.x + x * tileSize;
                        var hitX2 = hitX1;
                        var hitY1 = mapObject.y + y * tileSize;
                        var hitY2 = hitY1;
                        var hitW1 = tileSize;
                        var hitW2 = hitW1;
                        var hitH1 = tileSize;
                        var hitH2 = hitH1;
                        if (tileProps.collide == "CornerTL") {
                            hitH1 = Std.int(tileSize / 4);
                            hitW2 = hitH1;
                        }
                        else if (tileProps.collide == "CornerTR") {
                            hitW2 = Std.int(tileSize / 4);
                            hitH1 = hitW2;
                            hitX2 += tileSize - hitW2;
                        }
                        else if (tileProps.collide == "CornerBR") {
                            hitW2 = Std.int(tileSize / 4);
                            hitH1 = hitW2;
                            hitX2 += tileSize - hitW2;
                            hitY1 += tileSize - hitH1;
                        }
                        else if (tileProps.collide == "CornerBL") {
                            hitH1 = Std.int(tileSize / 4);
                            hitW2 = hitH1;
                            hitY1 += tileSize - hitH1;
                        }

                        var hitObject1 = new FlxObject(hitX1, hitY1, hitW1, hitH1);                        
                        hitObject1.immovable = true;
                        placeHitObjectFn(hitObject1);

                        var hitObject2 = new FlxObject(hitX2, hitY2, hitW2, hitH2);
                        hitObject2.immovable = true;
                        placeHitObjectFn(hitObject2);

                        continue;
                    }

                    var hitX = mapObject.x + x * tileSize;
                    var hitY = mapObject.y + y * tileSize;
                    var hitW = tileSize;
                    var hitH = tileSize;
                    if (tileProps.collide == "Full") {
                        hitW = hitH = tileSize;
                    }
                    else if (tileProps.collide == "Left") {
                        hitW = Std.int(tileSize / 4);
                    }
                    else if (tileProps.collide == "Right") {
                        hitW = Std.int(tileSize / 4);
                        hitX += tileSize - hitW;
                    }
                    else if (tileProps.collide == "Top") {
                        hitH = Std.int(tileSize / 4);
                    }
                    else if (tileProps.collide == "Bottom") {
                        hitH = Std.int(tileSize / 4);
                        hitY += tileSize - hitH;
                    }
                    else if (tileProps.collide == "Small") {
                        hitW = hitH = Std.int(tileSize / 2);
                        hitX += Std.int(hitW / 2);
                        hitY += Std.int(hitH / 2);
                    }
                    else if (tileProps.collide == "HalfTop") {
                        hitH = Std.int(tileSize / 2);
                    }
                    else if (tileProps.collide == "HalfRight") {
                        hitW = Std.int(tileSize / 2);
                        hitX += hitW;
                    }
                    else if (tileProps.collide == "HalfBottom") {
                        hitH = Std.int(tileSize / 2);
                        hitY += hitH;
                    }
                    else if (tileProps.collide == "HalfLeft") {
                        hitW = Std.int(tileSize / 2);
                    }
                    else if (tileProps.collide == "SmallTR") {
                        hitW = hitH = Std.int(tileSize / 2);
                        hitX += hitW;
                    }
                    else if (tileProps.collide == "SmallTL") {
                        hitW = hitH = Std.int(tileSize / 2);
                    }
                    else if (tileProps.collide == "SmallBL") {
                        hitW = hitH = Std.int(tileSize / 2);
                        hitY += hitH;
                    }
                    else if (tileProps.collide == "SmallBR") {
                        hitW = hitH = Std.int(tileSize / 2);
                        hitX += hitW;
                        hitY += hitH;
                    }
                    else {
                        // trace(tileProps.collide);
                        continue;
                    }
                    var hitObject = new FlxObject(hitX, hitY, hitW, hitH);                        
                    hitObject.immovable = true;
                    placeHitObjectFn(hitObject);
                }
            }
        }
    }
}