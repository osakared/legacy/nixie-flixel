/* 
 * Copyright 2017 Thomas J. Webb
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *         following disclaimer.
 *      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *         following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package nixie;

import flixel.input.gamepad.FlxGamepadAnalogStick;

/**
 * IDs for SNES controllers (key codes based on Cordless Rumblepad 2)
 */
class SNESID
{
	public static inline var ONE:Int = 0;
	public static inline var TWO:Int = 1;
	public static inline var THREE:Int = 2;
	public static inline var FOUR:Int = 3;
	public static inline var FIVE:Int = 4;
	public static inline var SIX:Int = 5;
	public static inline var SEVEN:Int = 6;
	public static inline var EIGHT:Int = 7;
	public static inline var NINE:Int = 8;
	public static inline var TEN:Int = 9;
	
	//"fake" IDs, we manually watch for hat axis changes and then send events using these otherwise unused joystick button codes
	public static inline var DPAD_UP:Int = 16;
	public static inline var DPAD_DOWN:Int = 17;
	public static inline var DPAD_LEFT:Int = 18;
	public static inline var DPAD_RIGHT:Int = 19;

	public static var LEFT_ANALOG_STICK(default, null)  = new FlxGamepadAnalogStick(3, 4, {up:24, down:25, left:26, right:27});

}