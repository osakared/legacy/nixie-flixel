/* 
 * Copyright 2017 Thomas J. Webb
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *         following disclaimer.
 *      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *         following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package nixie;

import flixel.FlxG;
import flixel.FlxBasic;
import flixel.FlxObject;
import flixel.input.gamepad.FlxGamepad;
import nixie.SNESMapping;

class PlayerDirectionalControl extends FlxBasic
{
    #if !FLX_NO_GAMEPAD
	public var gamepad:FlxGamepad;
    #end

	private var keyUp:Bool = false;
	private var keyDown:Bool = false;
	private var keyLeft:Bool = false;
	private var keyRight:Bool = false;
	private var keyB:Bool = false;

	private var padUp:Bool = false;
	private var padDown:Bool = false;
	private var padLeft:Bool = false;
	private var padRight:Bool = false;
	private var padB:Bool = false;

    private var controllables:Array<Controllable>;

	public function new() 
	{
		super();

        controllables = new Array<Controllable>();

		#if !FLX_NO_GAMEPAD
		gamepad = FlxG.gamepads.lastActive;
		if (gamepad != null) gamepad.mapping = new SNESMapping();
		#end
	}

    public function addControllable(controllable:Controllable):Void
    {
        controllables.push(controllable);
    }
	
	private function movement():Void
	{
		#if FLX_KEYBOARD
		keyUp = FlxG.keys.anyPressed([UP, W]);
		keyDown = FlxG.keys.anyPressed([DOWN, S]);
		keyLeft = FlxG.keys.anyPressed([LEFT, A]);
		keyRight = FlxG.keys.anyPressed([RIGHT, D]);
		#end

		#if !FLX_NO_GAMEPAD
		if (gamepad != null) {
			var pressed = gamepad.pressed;
			padB = pressed.B;

			if (gamepad.analog.justReleased.LEFT_STICK_X) {
				padLeft = padRight = false;
			}
			if (gamepad.analog.justReleased.LEFT_STICK_Y) {
				padUp = padDown = false;
			}

			if (gamepad.analog.justMoved.LEFT_STICK_X) {
				if (gamepad.analog.value.LEFT_STICK_X < 0) {
					padLeft = true;
					padRight = false;
				}
				else {
					padLeft = false;
					padRight = true;
				}
			}
			if (gamepad.analog.justMoved.LEFT_STICK_Y) {
				if (gamepad.analog.value.LEFT_STICK_Y < 0) {
					padUp = true;
					padDown = false;
				}
				else {
					padUp = false;
					padDown = true;
				}
			}
		}
		#end

		var up = padUp || keyUp;
		var down = padDown || keyDown;
		var left = padLeft || keyLeft;
		var right = padRight || keyRight;
		var b = padB || keyB;
		
		if (up && down) {
			up = down = false;
		}
		if (left && right) {
			left = right = false;
		}
		
		if (up || down || left || right) {
			var angle:Float = 0;
			if (up) {
				angle = -90;
				if (left)
					angle -= 45;
				else if (right)
					angle += 45;
			}
			else if (down) {
				angle = 90;
				if (left)
					angle += 45;
				else if (right)
					angle -= 45;
			}
			else if (left) {
				angle = 180;
			}
			else if (right) {
				angle = 0;
			}
			
            for (i in 0...controllables.length) {
                var controllable = controllables[i];
                var speed = b ? controllable.getRunningSpeed() : controllable.getWalkingSpeed();
                controllable.walk(angle, speed);
            }
		}
		else {
            for (i in 0...controllables.length) {
                controllables[i].stop();
            }
		}
	}
	
	override public function update(elapsed:Float):Void 
	{
		movement();
		super.update(elapsed);
	}
}
