/* 
 * Copyright 2017 Thomas J. Webb
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *         following disclaimer.
 *      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *         following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package nixie;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.group.FlxSpriteGroup;
import flixel.tile.FlxTilemap;
import flixel.util.FlxColor;
import flixel.util.FlxSort;
#if mobile
import flixel.ui.FlxVirtualPad;
#end

import nixie.FlxCastleLoader;

class PlayState extends FlxSubState
{
	private var player:Avatar;
	private var playerControl:PlayerDirectionalControl;
	private var map:FlxCastleLoader;
	private var background:FlxTilemap;
	private var objects:FlxSpriteGroup;
	private var hitObjects:FlxTypedGroup<FlxObject>;
	private var hitMap:FlxTilemap;
	private var data:Dynamic;
	
	#if mobile
	public static var virtualPad:FlxVirtualPad;
	#end

	override public function create():Void
	{
		#if FLX_MOUSE
		FlxG.mouse.visible = false;
		#end

		map = new FlxCastleLoader(data);
		background = map.loadTilemap("ground");
		background.follow();
		add(background);
		objects = new FlxSpriteGroup();
		add(objects);
		hitObjects = new FlxTypedGroup<FlxObject>();
		add(hitObjects);
		map.loadObjects("objects", placeObject, placeHitObject);
		hitMap = map.loadHitMap();
		// FlxG.debugger.drawDebug = true;
		add(hitMap);
		
		player = new Avatar();
		objects.add(player);

		playerControl = new PlayerDirectionalControl();
		add(playerControl);
		playerControl.addControllable(player);

		FlxG.camera.follow(player, TOPDOWN, 1);
		
		#if mobile
		virtualPad = new FlxVirtualPad(FULL, A_B);		
		add(virtualPad);
		#end
		
		FlxG.camera.fade(FlxColor.BLACK, .33, true);
		
		super.create();
	}

	public function new(_data:Dynamic)
	{
		super();
		data = _data;
	}

	private function placeObject(object:FlxSprite):Void
	{
		objects.add(object);
	}

	private function placeHitObject(object:FlxObject):Void
	{
		hitObjects.add(object);
	}

	private static function byPlace(order:Int, obj1:FlxObject, obj2:FlxObject):Int
	{
		return FlxSort.byValues(order, obj1.y + obj1.height, obj2.y + obj2.height);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);

		FlxG.collide(player, hitMap);
		FlxG.collide(player, hitObjects);

		objects.sort(byPlace, FlxSort.ASCENDING);
	}	
}
