/* 
 * Copyright 2017 Thomas J. Webb
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *         following disclaimer.
 *      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *         following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package nixie;

import flixel.input.gamepad.FlxGamepadInputID;
import flixel.input.gamepad.mappings.FlxGamepadMapping;

import nixie.SNESID;

class SNESMapping extends FlxGamepadMapping
{
    #if FLX_JOYSTICK_API
	private static inline var LEFT_ANALOG_STICK_FAKE_X:Int = 20;
	private static inline var LEFT_ANALOG_STICK_FAKE_Y:Int = 21;
	#end
	
	override function initValues():Void 
	{
		leftStick = SNESID.LEFT_ANALOG_STICK; 
	}
	
	override public function getID(rawID:Int):FlxGamepadInputID 
	{
        // if (rawID != -1) trace(rawID);
		return switch (rawID)
		{
			case SNESID.TWO: A;
			case SNESID.THREE: B;
			case SNESID.ONE: X;
			case SNESID.FOUR: Y;
			case SNESID.NINE: BACK;
			case SNESID.TEN: START;
			case SNESID.FIVE: LEFT_SHOULDER;
			case SNESID.SIX: RIGHT_SHOULDER;
			// case SNESID.DPAD_DOWN: DPAD_DOWN;
			// case SNESID.DPAD_LEFT: DPAD_LEFT;
			// case SNESID.DPAD_RIGHT: DPAD_RIGHT;
			// case SNESID.DPAD_UP: DPAD_UP;
			case id if (id == leftStick.rawUp): LEFT_STICK_DIGITAL_UP;
			case id if (id == leftStick.rawDown): LEFT_STICK_DIGITAL_DOWN;
			case id if (id == leftStick.rawLeft): LEFT_STICK_DIGITAL_LEFT;
			case id if (id == leftStick.rawRight): LEFT_STICK_DIGITAL_RIGHT;
			case _: NONE;
		}
	}

	override public function getRawID(ID:FlxGamepadInputID):Int 
	{
		return switch (ID)
		{
			case A: SNESID.TWO;
			case B: SNESID.THREE;
			case X: SNESID.ONE;
			case Y: SNESID.FOUR;
			case BACK: SNESID.NINE;
			case START: SNESID.TEN;
			case LEFT_SHOULDER: SNESID.FIVE;
			case RIGHT_SHOULDER: SNESID.SIX;
			// case DPAD_UP: SNESID.DPAD_UP;
			// case DPAD_DOWN: SNESID.DPAD_DOWN;
			// case DPAD_LEFT: SNESID.DPAD_LEFT;
			// case DPAD_RIGHT: SNESID.DPAD_RIGHT;
			case LEFT_STICK_DIGITAL_UP: SNESID.LEFT_ANALOG_STICK.rawUp;
			case LEFT_STICK_DIGITAL_DOWN: SNESID.LEFT_ANALOG_STICK.rawDown;
			case LEFT_STICK_DIGITAL_LEFT: SNESID.LEFT_ANALOG_STICK.rawLeft;
			case LEFT_STICK_DIGITAL_RIGHT: SNESID.LEFT_ANALOG_STICK.rawRight;
			default: -1;
		}
	}
	
	#if FLX_JOYSTICK_API
	override public function axisIndexToRawID(axisID:Int):Int 
	{
		return if (axisID == leftStick.x) LEFT_ANALOG_STICK_FAKE_X;
			else if (axisID == leftStick.y) LEFT_ANALOG_STICK_FAKE_Y;
			else -1;
	}
 #end
}