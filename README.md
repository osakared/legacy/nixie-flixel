# Nixie

This is a no-frills 2D RPG/Adventure/Visual Novel engine. Game logic is pure haxe and presentation is based on [HaxeFlixel](http://haxeflixel.com/).

The aim is to have the classes as much as possible usable by themselves so that functionality can be integrated whether or not you want to base your game entirely on the engine. [CastleDB](http://castledb.org/), which is simply a json file, is used for metadata and map layouts. As much as possible, classes that get data from CastleDB shall also work with appropriately structured `Dynamic` objects.

## Platforms Tested (so far)

- Native
  - Mac OS X
- HTML5
- Flash

## Authors

- Thomas J. Webb (Osaka Red, LLC and Surge Consulting)

## License

The source code is free to incorporate into your open-source or closed-source project per the terms of the 2-clause BSD license. See COPYING.

The following images are authored by Leonard Pabin and redistributable per the terms of Creative Commons [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/):

- assets/images/ground_tiles.png
- assets/images/object_layer.png
- assets/images/treesv6_0.png
